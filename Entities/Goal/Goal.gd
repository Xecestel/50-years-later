extends Area2D

func _on_Goal_body_entered(body):
	if ("Player" in body.name):
		body.set_right_place(true);


func _on_Goal_body_exited(body):
	if ("Player" in body.name):
		body.set_right_place(false);
