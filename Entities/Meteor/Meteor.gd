extends KinematicBody2D

#constants#
const UP = Vector2(0, -1);

#variables#
export var speed = 200;
export var direction = Vector2.ZERO;
onready var spawn_pos = self.position;
var motion = Vector2.ZERO;
var processing = false;
var respawning = false;
###########

#signals#
signal meteor_hit;
########

func _ready():
	$Sprite.play("default");
	var animation = "rotate-clock" if direction.x > 0 else "rotate-anticlock";
	$Animation.play(animation);
	var player = self.get_parent().get_node("Player");
	self.connect("meteor_hit", player, "_on_meteor_hit");

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (self.processing == true && self.visible):
		motion = direction * speed;

		if (self.position.x <= -200 || self.position.x >= 2000):
			self.respawn();
		
		self.move_and_slide(motion, UP);
		
	elif ($RespawnTimer.time_left == 0 && self.respawning):
		self.visible = true;
		self.processing = true;
		$Area2D.monitoring = true;
		self.respawning = false;


func _on_Area2D_body_entered(body):
	if (self.visible == false || self.processing == false):
		return;
		
	if ("Player" in body.name):
		$Animation.stop();
		emit_signal("meteor_hit");
		self.processing = false;
		$Sprite.play("explosion");
	elif ("TileMap" in body.name):
		self.respawn();
	else:
		return;


func _on_Player_start():
	self.processing = true;
	
func respawn():
	$RespawnTimer.start();
	self.visible = false;
	self.processing = false;
	self.global_position = spawn_pos;
	self.respawning = true;
	$Area2D.monitoring = false;


func _on_Animation_animation_finished(anim_name):
	if (self.processing):
		$Animation.play(anim_name);
