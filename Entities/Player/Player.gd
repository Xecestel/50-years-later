extends KinematicBody2D

#constants#
const GRAVITY = 3.3;
const MAX_PROPELLERS = 300;
const PROPELLERS_ACCELERATION = 10;
const UP = Vector2(0, -1);
const FUEL_CONSUMPTION = 1;
###########

#variables#
var motion = Vector2.ZERO;
var right_place = false;
var start = false;
var fuel = 300;
var goal : Node;
var hud : Node;
var victory_flag;
###########

#signals#
signal start;
signal victory;
#########

func _ready():
	SoundManager.play_bgm("BGM");
	self.connect_nodes();
#end
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	self.get_input();
	self.move_goal_pointer();
		
	if (start):
		self.execute_movement();
		self.check_collisions();	
#end

###########################



#########################
"""INPUT AND MOVEMENT"""
#########################

func get_input() -> void:
	if (Input.is_action_pressed("propellers") && fuel > 0):
		$Sprite.play("flying");
		SoundManager.play_se("Propellers");
		start = true;
		emit_signal("start");
		$Instructions.visible = false;
		var seno = sin($Propellers_Direction.cast_to.angle());
		var coseno = cos($Propellers_Direction.cast_to.angle());
		var propellers_speed = $Propellers_Direction.cast_to * PROPELLERS_ACCELERATION;
		
		if (coseno > 0):
			motion.x = min(motion.x + propellers_speed.x, MAX_PROPELLERS);
		elif (seno == 0):
			pass;
		else:
			motion.x = max(motion.x + propellers_speed.x, -MAX_PROPELLERS);

		if (seno < 0):
			motion.y = max(motion.y + propellers_speed.y, -MAX_PROPELLERS);
		elif(seno == 0):
			pass;
		else:
			motion.y = min(motion.y + propellers_speed.y, MAX_PROPELLERS);
			
		fuel = max (fuel - FUEL_CONSUMPTION, 0);
		if (hud != null):
			hud.update_fuel(fuel);
		
	else:
		SoundManager.stop_se("Propellers");
		$Sprite.play("idle");
		motion.x = lerp(motion.x, 0, 0.005);
		
	if (Input.is_action_pressed("right")):
		$Sprite.rotation_degrees += 1;
		$CollisionShape2D.rotation_degrees += 1;
		$FloorRayCast.cast_to = rotate_vector($FloorRayCast.cast_to, 1);
		$FloorRayCast2.cast_to = rotate_vector($FloorRayCast2.cast_to, 1);
		$Propellers_Direction.cast_to = rotate_vector($Propellers_Direction.cast_to, 1);
		
	if (Input.is_action_pressed("left")):
		$Sprite.rotation_degrees -= 1;
		$CollisionShape2D.rotation_degrees -= 1;
		$FloorRayCast.cast_to = rotate_vector($FloorRayCast.cast_to, -1);
		$FloorRayCast2.cast_to = rotate_vector($FloorRayCast2.cast_to, -1);
		$Propellers_Direction.cast_to = rotate_vector($Propellers_Direction.cast_to, -1);
#end

#Allows the player to reset the game even if the Player node is not
#processing
func _input(event):
	if (event.is_action_pressed("reset")):
		self.get_tree().reload_current_scene();
	
	if (event.is_action_pressed("fullscreen")):
		hud.fullscreen();
		
	if (event.is_action_pressed("mute")):
		hud.mute_game();
#end

#Executes movement adding gravity and calling move_and_slide
func execute_movement() -> void:
	motion.y += GRAVITY;
	self.move_and_slide(motion, UP);
#end

#Takes a vector, rotates and returns the rotation
func rotate_vector(vector : Vector2, degrees : float) -> Vector2:
	var new_x;
	var new_y;

	new_x = vector.x * cos(degrees*PI/180) - vector.y * sin(degrees*PI/180);
	new_y = vector.x * sin(degrees*PI/180) + vector.y * cos(degrees*PI/180);
	
	vector.x = new_x;
	vector.y = new_y;
	
	return vector;
#end

#Moves the GoalPointer node to follow the Goal on the screen.
func move_goal_pointer():
	if (goal == null):
		return;
		
	var distance = goal.position - self.position;
	$GoalDirection.cast_to = distance.normalized();
	$GoalPointer.position = $GoalDirection.cast_to * 64;
	$GoalPointer.rotation = $GoalDirection.cast_to.angle() - PI/2;
#end

#########################

#Connects Player node to other nodes through signals and methods
func connect_nodes() -> void:
	victory_flag	=	self.get_parent().get_node("VictoryFlag");
	hud				=	self.get_parent().get_node("HUD");
	goal			=	self.get_parent().get_node("Goal");
	
	self.connect("start", hud, "_on_Player_start");
	
	for node in self.get_parent().get_children():
		if ("Meteor" in node.name):
			self.connect("start", node, "_on_Player_start");
#end


#Checks if the Player is colliding with floors and walls.
#It also checks if the player reached its goal or not.
func check_collisions() -> void:
	if (self.is_on_floor()):
			if ($FloorRayCast.is_colliding() && $FloorRayCast2.is_colliding() && self.right_place):
				SoundManager.play_me("Victory");
				self.set_physics_process(false);
				if (self.get_parent().name == "Level6"):
					$CanvasLayer/Victory.text = "Victory!\nThanks for playing!\nPress Enter to Restart the game"
				$CanvasLayer/Victory.visible = true;
				self.get_tree().paused = true;
				self.victory_flag.process_victory();
			else:
				self.game_over();
	if (self.is_on_wall()):
		self.game_over();
		

#end
func game_over():
	SoundManager.play_se("Explosion");
	self.set_physics_process(false);
	$CanvasLayer/GameOver.visible = true;
#end

func set_right_place(is_right : bool) -> void:
	self.right_place = is_right;


func _on_meteor_hit():
	self.game_over();
