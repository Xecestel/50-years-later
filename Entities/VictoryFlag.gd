extends Sprite

#constants#
const LEVELS_DIR_PATH = "res://Scenes/"
###########

#variables#
export (int) var next_level = 1;
###########


func _ready():
	self.get_tree().paused = false;

func _input(event):
	if (self.visible == false):
		return;
	
	if (event.is_action_pressed("next_level")):
		var level_name = "Level" + str(next_level) + ".tscn";
		self.get_tree().change_scene(LEVELS_DIR_PATH + level_name);

func process_victory() -> void:
	self.visible = true;
