# 50 Years Later

## About the Game

2D Pixel Art Casual Arcade game made by Xecestel (Celeste Privitera).
In this game you control the Apollo XI Lander. Your objective is to land on the right spot whitout crashing on the moon surface.
It's just a little game I made as a tribute for the fiftieth anniversary of the Apollo XI mission.

[Play this game on itch.io!](https://xecestel.itch.io/50-years-later)

## Instructions

Hold Spacebar to fire the lander's propellers. Watch out the fuel level on the top left corner of the screen!
Hold Left/Right to rotate the lander Left/Right.
Land on the red circle on the moon surface with both feet on the ground.
Avoid crashing to the ground or to the meteorites!

## Licenses

50 Years Later
Copyright (C) 2019  Celeste Privitera

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can find the source code of this game [here](https://gitlab.com/Xecestel/50-years-later).

The graphic assets made by Xecestel (Celeste Privitera) are licensed under the Creative Commons Attribution-ShareAlike 4.0 Unported License.
To view a copy of this license, visit [this page](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
or read "LICENSE_CC-BY-SA.txt" in this directory.

Said assets are:
- "FuelBar", "Lander", "TileSet", "Meteor", "Icons" directories content
- "Flag.png"

Third parties assets are licensed under licenses chose by their author, as listed:

Art:
	"gitlab.png"
	License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)

Fonts:
	
	"RitzyNormal.ttf" (https://www.1001fonts.com/ritzynormal-font.html)
	by Nick Curtis
	License: FFC (1001Fonts Free For Commercial Use License)

	"Para Aminobenzoic Font" (https://www.1001fonts.com/paraaminobenzoic-font.html)
	by Raymond Larabie
	License: Typodermic Fonts Inc. End User License Agreement



The "LICENSE" file refers to all parts of the code made specifically for this game by Xecestel (Celeste Privitera).
For the licenses of third party assets and codes, please refer to the documentation files found on the same directory of the assets.


## Changelog

### Version 1.0

- Added Licenses and Readme files.

### Version 1.2

- Added meteorites as new obstacles.

### Version 1.5

- Added new levels (Level1 to Level6).
- Added the possibility to go to the next level in VictoryFlag code.
- Added Mute and Fullscreen buttons.
- Added new animations for meteors.
- Added Credits.txt.
- Fixed minor bugs in Goal and Meteor codes.

### Version 1.6

- Added GoalIndicator to point the player towards the Goal.
- Changed Meteors color to make them more visible.

### Version 1.6.1

- Fixed a bug in Meteor objects which caused them to start processing before becoming visible after respawn
- Minor optimizations

### Version 1.7

- Added SoundManager to manage background music.
- Minor optimizations.

### Version 1.7.1

- Fixed a bug: BGM was not imported as a loop

### Version 1.8

- Replaced previous SoundManager with the new [Sound Manager Module](https://gitlab.com/Xecestel/sound-manager)

### Version 1.8.1

- Updated Sound Manager Module at version 1.5.2

### Version 1.8.2

- Updated Sound Manager Module at version 1.6

### Version 1.8.3

- Updated Sound Manager Module at version 1.7

### Version 1.8.4

- Updated Sound Manager Module at version 1.8

### Version 1.9

- Replaced Sound Manager Module with Sound Manager Plugin
- Bug fixes and optimizations

### Version 1.10
- Updated Sound Manager Plugin to latest version (1.10.1)
- Bug fixes
