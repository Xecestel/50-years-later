extends CanvasLayer

#variables#
export (bool) var first_level = false;
###########

func _ready():
	if (not first_level):
		self.hide_title();
	$HBoxContainer/FullscreenBtn.pressed = OS.window_fullscreen;
	$HBoxContainer/MuteBtn.pressed = AudioServer.is_bus_mute(0);
	
		
func hide_title() -> void:
	$Title.visible = false;
	$Copyright.visible = false;
	$GitLab.visible = false;
	
func update_fuel(fuel):
	$FuelBar.value = fuel;


func fullscreen() -> void:
	var is_fullscreen = OS.window_fullscreen;
	$HBoxContainer/FullscreenBtn.pressed = !is_fullscreen;
	self._on_FullscreenBtn_toggled(!is_fullscreen);
	
	
func mute_game() -> void:
	var is_mute = AudioServer.is_bus_mute(0);
	$HBoxContainer/MuteBtn.pressed = !is_mute;
	self._on_MuteBtn_toggled(!is_mute);


func _on_Player_start():
	self.hide_title();


func _on_FullscreenBtn_toggled(button_pressed):
	OS.window_fullscreen = button_pressed;
	$HBoxContainer/FullscreenBtn.release_focus();


func _on_MuteBtn_toggled(button_pressed):
	AudioServer.set_bus_mute(0, button_pressed);
	$HBoxContainer/MuteBtn.release_focus();


func _on_GitLab_pressed():
	OS.shell_open("https://gitlab.com/Xecestel/50-years-later");
