Written, designed and developed by Xecestel.

Proudly made with Godot Engine 3.


Fonts:
	
	"RitzyNormal.ttf" (https://www.1001fonts.com/ritzynormal-font.html)
	by Nick Curtis
	License: FFC (1001Fonts Free For Commercial Use License)

	"Para Aminobenzoic Font" (https://www.1001fonts.com/paraaminobenzoic-font.html)
	by Raymond Larabie
	License: Typodermic Fonts Inc. End User License Agreement


Thanks to everyone who helped me and supported me during development.

Thanks to you for playing this game!
